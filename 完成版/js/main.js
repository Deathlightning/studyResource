const url = "http://10.32.6.132:8080";

function getClassifyList() {
    var classifyList;
    var length = 0;
    getData().then(res => {
        console.log(res)
        const classifyList = res;
        var htmlText = "";
        for (var i = 0; i < classifyList.length; i++) {
            htmlText = htmlText + "<li><a id=" + classifyList[i] + "  onclick=getContent('" + classifyList[i] + "')>" + classifyList[i] + "</a>";
        }
        document.getElementById("classifyList").innerHTML = htmlText;
        console.log(htmlText != "");
    }).catch(err => {
        console.error(err);
    })

}
//处理异步请求
function getData() {
    return new Promise((resolve, reject) => {
        axios.get(url + "/getContentList")
            .then(function(response) {
                resolve(response.data);
            })
            .catch(function(error) {
                reject(error);
            });
    })

}

//导航按钮切换
function chosen(btId) {
    var a = document.getElementById("nav").getElementsByTagName("button");
    for (i = 0; i < a.length; i++) {
        a[i].classList.remove("buttomBorder");
    }
    document.getElementById("bt" + btId).classList.add("buttomBorder");
    getContent(btId);
}

//根据classify读取content
function getContent(classify) {
    console.log("请求classify：" + classify);
    var parmeter = {};
    parmeter.classify = classify;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url + "/", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp.send(JSON.stringify(parmeter));
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("content").innerHTML = xmlhttp.responseText;
        }
    }
}

//往数据库插入content
function btInsert() {
    var content = editor.txt.html();
    var classify = document.getElementById("newClssify").value;
    if (classify == null || classify == "" || classify == "undefined") {
        alert("请填写文章类别" + classify);
        return "";
    }
    console.log(content);
    console.log(classify);
    var parmeter = {};
    parmeter.classify = classify;
    parmeter.content = content;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url + "/insert", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            alert("新增成功");
        }
    }
    xmlhttp.send(JSON.stringify(parmeter));
}